import Swal from "sweetalert2/dist/sweetalert2.js"

const propertiesToSet=[
    "$confirm",
    "$alert",
    "$error",
    "$warn",
    "$fire",
]

export default class VueSimpleAlert {
    static install(vue){
        const instance=new VueSimpleAlert()
        vue.prototype.$VSA=instance
        for(let property of propertiesToSet){
            if(!(property in vue.prototype)){
                vue.prototype[property]=instance[property.slice(1)].bind(instance)
            }
        }
    }

    fire(...options){
        return Swal.fire(...options)
    }

    confirm(text, title, icon, options){
        return this.fire({
            text,
            title,
            icon: icon||"question",
            showCancelButton: true,
            ...options,
        }).then(r=>{
            if(r?.value) return r.value
            throw (r.dismiss||r.value)
        })
    }

    alert(text, title, icon, options){
        return this.fire({
            title,
            text,
            icon,
            ...options,
        })
    }

    error(text, title, options){
        return this.fire({
            ...options,
            icon: "error",
        })
    }

    warn(text, title, options){
        return this.fire({
            icon: "warning",
            ...options,
        })
    }
}

