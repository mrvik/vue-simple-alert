module.exports = {
    root: true,
    ecmaVersion: 8,
    env: {
        webpack: true,
    },
    extends: [
      "plugin:prettier/recommended",
    ],
    rules: {
      "no-console": "error",
      "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    },
};
